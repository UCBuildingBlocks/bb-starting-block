<%@ page isErrorPage="true" %>

<%@ taglib uri="/bbUI"   prefix="bbUI" %>
<%@ taglib uri="/bbData" prefix="bbData" %>

<bbData:context>
  <bbUI:docTemplate>

    <bbUI:error exception="<%=exception%>"/>

  </bbUI:docTemplate>
</bbData:context>
