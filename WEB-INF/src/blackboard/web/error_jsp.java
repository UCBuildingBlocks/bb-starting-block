package blackboard.web;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class error_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    Throwable exception = org.apache.jasper.runtime.JspRuntimeLibrary.getThrowable(request);
    if (exception != null) {
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      //  bbData:context
      blackboard.servlet.tags.data.ContextTag _jspx_th_bbData_005fcontext_005f0 = new blackboard.servlet.tags.data.ContextTag();
      org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_bbData_005fcontext_005f0);
      _jspx_th_bbData_005fcontext_005f0.setPageContext(_jspx_page_context);
      _jspx_th_bbData_005fcontext_005f0.setParent(null);
      int[] _jspx_push_body_count_bbData_005fcontext_005f0 = new int[] { 0 };
      try {
        int _jspx_eval_bbData_005fcontext_005f0 = _jspx_th_bbData_005fcontext_005f0.doStartTag();
        if (_jspx_eval_bbData_005fcontext_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
          blackboard.platform.context.Context bbContext = null;
          bbContext = (blackboard.platform.context.Context) _jspx_page_context.findAttribute("bbContext");
          do {
            //  bbUI:docTemplate
            blackboard.servlet.tags.ProductionDocTemplateTag _jspx_th_bbUI_005fdocTemplate_005f0 = new blackboard.servlet.tags.ProductionDocTemplateTag();
            org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_bbUI_005fdocTemplate_005f0);
            _jspx_th_bbUI_005fdocTemplate_005f0.setPageContext(_jspx_page_context);
            _jspx_th_bbUI_005fdocTemplate_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_bbData_005fcontext_005f0);
            int _jspx_eval_bbUI_005fdocTemplate_005f0 = _jspx_th_bbUI_005fdocTemplate_005f0.doStartTag();
            if (_jspx_eval_bbUI_005fdocTemplate_005f0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
              do {
                //  bbUI:error
                blackboard.servlet.tags.ErrorTag _jspx_th_bbUI_005ferror_005f0 = new blackboard.servlet.tags.ErrorTag();
                org.apache.jasper.runtime.AnnotationHelper.postConstruct(_jsp_annotationprocessor, _jspx_th_bbUI_005ferror_005f0);
                _jspx_th_bbUI_005ferror_005f0.setPageContext(_jspx_page_context);
                _jspx_th_bbUI_005ferror_005f0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_bbUI_005fdocTemplate_005f0);
                // /error.jsp(9,4) name = exception type = null reqTime = true required = true fragment = false deferredValue = false expectedTypeName = null deferredMethod = false methodSignature = null
                _jspx_th_bbUI_005ferror_005f0.setException(exception);
                int _jspx_eval_bbUI_005ferror_005f0 = _jspx_th_bbUI_005ferror_005f0.doStartTag();
                if (_jspx_th_bbUI_005ferror_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
                  _jspx_th_bbUI_005ferror_005f0.release();
                  org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_bbUI_005ferror_005f0);
                  return;
                }
                _jspx_th_bbUI_005ferror_005f0.release();
                org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_bbUI_005ferror_005f0);
                int evalDoAfterBody = _jspx_th_bbUI_005fdocTemplate_005f0.doAfterBody();
                if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
                  break;
              } while (true);
            }
            if (_jspx_th_bbUI_005fdocTemplate_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
              _jspx_th_bbUI_005fdocTemplate_005f0.release();
              org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_bbUI_005fdocTemplate_005f0);
              return;
            }
            _jspx_th_bbUI_005fdocTemplate_005f0.release();
            org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_bbUI_005fdocTemplate_005f0);
            int evalDoAfterBody = _jspx_th_bbData_005fcontext_005f0.doAfterBody();
            bbContext = (blackboard.platform.context.Context) _jspx_page_context.findAttribute("bbContext");
            if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
              break;
          } while (true);
        }
        if (_jspx_th_bbData_005fcontext_005f0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
          return;
        }
      } catch (Throwable _jspx_exception) {
        while (_jspx_push_body_count_bbData_005fcontext_005f0[0]-- > 0)
          out = _jspx_page_context.popBody();
        _jspx_th_bbData_005fcontext_005f0.doCatch(_jspx_exception);
      } finally {
        _jspx_th_bbData_005fcontext_005f0.doFinally();
        _jspx_th_bbData_005fcontext_005f0.release();
        org.apache.jasper.runtime.AnnotationHelper.preDestroy(_jsp_annotationprocessor, _jspx_th_bbData_005fcontext_005f0);
      }
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
